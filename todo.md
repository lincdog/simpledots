# TODO

## WBImage
### Short-term
* Handle shape changes in `apply` correctly
    * If shape changes, remake WBImage object to update dimension attributes
* `apply` analogs that act over each channel or each slice (for example, for correcting chromatic aberration, Z projecting)

### Long-term
* Try implementing with `xarray` instead of Numpy arrays
    * will allow named axes and avoid ordering issues
    * more complicated to iterate over all 2D images?

## DotImage
### Short-term
* Try a few different dot-finding routines, e.g. blob detection algorithms from `scikit-image`, on dense test data
    * Quantify blob sizes?
* Clean up and improve basic documentation
* Loading previous results
    * Read in a `dots_df`-like DataFrame, construct a `DotImage` from it (at least `local_max_image`) to allow further processing such as `coloc`. Possibly also generate simulated real image using Gaussian PSF of given width.

### Long-term
* Implement recording of processing steps, allowing you to reprocess or process another image identically
* Segmentation
    * connect to ilastik?
    * Is it possible to call ilastik from python? 
    * is there any way to do simple segmentation for cell culture without ilastik?
* Explore colocalization algorithms such as graph-based in new `skimage.graph` module
* Automated thresholding: Gaussian fits to histogram, or trial and error with second derivative test

## Other
* Implement multi-image container class for whole experiments
    * Array of `DotImage`s
    * Could implement `fetch_image`